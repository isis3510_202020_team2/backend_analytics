const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();


exports.addUser = functions.https.onRequest(async (req, res) => {
    // Grab the text parameter.
    const brig = req.query.brigadista;
    const id= req.query.id;
    const collection= req.query.collection;
    // Push the new message into Cloud Firestore using the Firebase Admin SDK.
    const writeResult = await admin.firestore().collection(collection).doc(id).set(brig);
    // Send back a message that we've succesfully written the message
    res.json({result: `Brigadista with ID: ${id} added.`});
  });


  
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

